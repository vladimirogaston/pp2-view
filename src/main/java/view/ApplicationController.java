package view;

import executor.Executor;
import executor.exceptions.ExecutionException;
import executor.exceptions.InvalidCommandException;

public class ApplicationController {

    private static final String EXECUTION_FAIL = "command execution status [fails]";

    private ApplicationView view;

    private Executor commandExecutor;

    public ApplicationController(ApplicationView applicationView, Executor commandExecutor) {
        this.view = applicationView;
        this.commandExecutor = commandExecutor;
    }

    public void actionPerformed() {
        String input = view.getInput();
        try {
            commandExecutor.execute(input);
            view.clear();
        } catch(InvalidCommandException | IllegalArgumentException | ExecutionException exception) {
            view.display(EXECUTION_FAIL + " " + exception.getMessage());
        }
    }
}